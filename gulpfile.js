var gulp = require("gulp");
var browserify = require("browserify");
var source = require("vinyl-source-stream");
var buffer = require("vinyl-buffer");
var webserver = require("gulp-webserver");
var sass = require("gulp-sass");
sass.compiler = require("node-sass");
var clean = require("gulp-clean");
var plumber = require("gulp-plumber");

var ts = require("gulp-typescript");
// var tsProject = ts.createProject('tsconfig.json');
var tsProject = ts.createProject({
  lib: ["es6", "dom"],
  module: "commonjs",
  target: "es6",
  sourceMap: true,
  allowJs: true,
  moduleResolution: "node",
  jsx: "react",
  moduleResolution: "node",
  noImplicitReturns: true,
  noImplicitThis: true,
  noImplicitAny: true,
  strictNullChecks: true
});

var SRC_DIR = "src/";
var DIST_DIR = "dist/";
var BUILD_DIR = "build/";
var BUILD_ASSETS_DIR = DIST_DIR + "assets/";

var npmList = ["react", "react-dom", "react-router-dom"];

function compile() {
  var bundler = browserify(SRC_DIR + "index.js");

  return bundler
    .transform("babelify", {
      presets: ["es2015", "react"]
    })
    .bundle()
    .pipe(source("app.js"))
    .pipe(buffer())
    .pipe(gulp.dest(DIST_DIR));
}

gulp.task("build:html", function() {
  return gulp
    .src(SRC_DIR + "**/*.html", SRC_DIR + "manifest.json", "!node_modules/**/*")
    .pipe(gulp.dest(DIST_DIR));
});

gulp.task("build:static", function() {
  return gulp.src("app/assets/**/*.*").pipe(gulp.dest(BUILD_ASSETS_DIR));
});

gulp.task("build:js", function() {
  return compile();
});

gulp.task("build:scss", function() {
  return gulp
    .src(SRC_DIR + "/styles/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest(DIST_DIR));
});

gulp.task("build", gulp.parallel("build:js", "build:html", "build:scss"));

gulp.task("webserver", function() {
  return gulp.src(DIST_DIR).pipe(
    webserver({
      livereload: true // reload browser page if something in DIST_DIR updates
    })
  );
});

gulp.task("watch:scss", function() {
  gulp.watch("./sass/**/*.scss", ["sass"]);
});

// ts build pipeline
function compileTs() {
  return gulp
    .src([SRC_DIR + "**/*.ts", SRC_DIR + "**/*.tsx"])
    .pipe(plumber())
    .pipe(tsProject())
    .js.pipe(gulp.dest(BUILD_DIR));
}

gulp.task("ts:transpile", function() {
  return compileTs();
});

gulp.task("ts:clean", function() {
  return gulp.src(DIST_DIR + "**/*").pipe(clean());
});

gulp.task("ts:bundle", function() {
  var bundler = browserify({ entries: [BUILD_DIR + "index.jsx"] });

  return (
    bundler
      .exclude(npmList)
      .bundle()
      .pipe(source("app.js"))
      .pipe(buffer())
      .pipe(gulp.dest(DIST_DIR))
  );
});

gulp.task(
  "build_v2",
  gulp.series(["build:html", "build:scss", "ts:transpile", "ts:bundle"])
);

// vendor build
gulp.task("build:vendor", function() {
  return browserify({ require: npmList })
    .transform("browserify-css", { global: true })
    .bundle()
    .pipe(source("vendor.js"))
    .pipe(buffer())
    .pipe(gulp.dest(DIST_DIR));
});
