import * as React from 'react';
import * as ReactDOM from "react-dom";
import App from './App';

window.onload = () => {
  var root = document.createElement("div");
  root.id = "react-root";
  document.body.appendChild(root);

  ReactDOM.render(React.createElement(App), root);
};
