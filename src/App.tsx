import * as React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from "./components/Pages/Home";
import About from "./components/Pages/About";

export default class App extends React.Component {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <Router>
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
      </Router>
    );
  }
}
