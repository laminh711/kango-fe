# Kango - Web Application for managing Kanban Board

# Local Development
Build and bundle:
```
gulp build_v2
```
Run local server:
```
gulp webserver
```