(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const react_router_dom_1 = require("react-router-dom");
const Home_1 = require("./components/Pages/Home");
const About_1 = require("./components/Pages/About");
class App extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (React.createElement(react_router_dom_1.BrowserRouter, null,
            React.createElement(react_router_dom_1.Link, { to: "/" }, "Home"),
            React.createElement(react_router_dom_1.Link, { to: "/about" }, "About"),
            React.createElement(react_router_dom_1.Route, { exact: true, path: "/", component: Home_1.default }),
            React.createElement(react_router_dom_1.Route, { path: "/about", component: About_1.default })));
    }
}
exports.default = App;

},{"./components/Pages/About":2,"./components/Pages/Home":3,"react":undefined,"react-router-dom":undefined}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
class About extends React.Component {
    render() {
        return React.createElement("div", { className: "slardar" }, " slardar");
    }
}
exports.default = About;

},{"react":undefined}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
class Home extends React.Component {
    render() {
        return (React.createElement("div", { className: "tiny" }, "tiny"));
    }
}
exports.default = Home;

},{"react":undefined}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const ReactDOM = require("react-dom");
const App_1 = require("./App");
window.onload = () => {
    var root = document.createElement("div");
    root.id = "react-root";
    document.body.appendChild(root);
    ReactDOM.render(React.createElement(App_1.default), root);
};

},{"./App":1,"react":undefined,"react-dom":undefined}]},{},[4]);
